package hajszan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@ComponentScan({"hajszan"})
public class EmployeeController {
		
		public static final String HTML_START="<html><body>";
	    public static final String HTML_END="</body></html>";
	    
	    @Autowired
		public UserService users;
	    
		 @RequestMapping("/employee")
		    public String greeting(@RequestParam(value="name", required=false, defaultValue="all") String name, Model model) {
			 
				 if(name.equals("all"))
				 {
					 StringBuilder emps = new StringBuilder();
					 for(User s : users.findAll())
					 {
						 emps.append(s.getFirstname() + " "+s.getLastname() + "<br/>");
					 }
					 model.addAttribute("employees", emps.toString());
				 }
			     model.addAttribute("name", name);
			     return "employees";
		    }
		    
}
